#!/usr/bin/env python

from optparse import OptionParser

import socket
import sys

parser = OptionParser()

parser.add_option("-i", "--id",
					help="sensor's ID (obligatorio)")
parser.add_option("-t", "--temperature",
					help="sensor de temperatura")	
parser.add_option("-l", "--light",
					help="sensor de luz")	
parser.add_option("-s", "--sound",
					help="sensor de sonido")	

(options, args) = parser.parse_args()


if(not options.id):
	print("Use -i <ID name> or --id <ID name> (ex. --id 1) to specify the sensor's name")
else:

	myLocalhost = 'localhost'
	ID = options.id

	ID_name="ID_"+ID
	seq = (ID_name, ID);
	if (options.temperature):
		variable = options.temperature
		nombre = 'temperatura'
		seq = seq + (nombre, variable);
	if (options.light):
		variable = options.light
		nombre = 'luz'
		seq = seq + (nombre, variable);

	if (options.sound):
		variable = options.sound
		nombre = 'sonido'
		seq = seq + (nombre, variable);

	st = " ";

	print st.join(seq)
	mensaje = st.join(seq)
	print mensaje

	host = myLocalhost
	#host = 'localhost'
	port = 56000
	size = 1024
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host,port))
	s.send(mensaje)
	s.close()

