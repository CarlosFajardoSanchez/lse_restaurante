#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OneWire.h>  //Para temperatura
#include <DallasTemperature.h> //Para temperatura

int gasPin = D2;

int DS18S20_Pin = D3; //Pin sensor DS18S20 (Tº).
OneWire ds(DS18S20_Pin);
DallasTemperature sensors (&ds);

/* Red WiFi */

char ssid[] = "Especial_MCHP";
char pass[] = "M15SEB304";

/*
char ssid[] = "MOVISTAR_8EA9";
char pass[] = "Db7ZK6tvTccWbCNCueHv";
*/
/*
char ssid[] = "MOVISTAR_7C20";
char pass[] = "5644B41E805A8398233B";
*/

unsigned int localPort = 80; //Puerto de llegada para socket
char packetBuffer[10]; //Donde se almacena el mensaje recibido
char packetSend[15];
String stringSend;

float getTemperatura();
float getRuido();
float gasExceed();
void setWifi();
void sendInfo();
char textoTempActual[6];
char textoRuidoActual[6];
char textoGasActual[6];
const int sleepTimeS = 10;


IPAddress servidorIP(192, 168, 1, 44); //IP raspi mesa

WiFiUDP Udp; //Variable para el socket

void setup() {

  pinMode(gasPin, INPUT);
  //pinMode(exceedDetected, OUTPUT);  
  
  Serial.begin(115200);
  setWifi();
  Serial.println("WiFi conectado."); //Informamos de que nos hemos conectado.
  Serial.print("Use esta URL para conectar: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

  Udp.begin(localPort);

  sendInfo();

  delay(200);

  sendInfo();

  ESP.deepSleep(sleepTimeS * 1000000);
}

void loop() {
}

void sendInfo() {
  int packetSize = Udp.parsePacket();
  dtostrf(getTemperatura(), 5, 2, textoTempActual);
  dtostrf(getRuido(), 5, 2, textoRuidoActual);
  dtostrf(gasExceed(), 5, 2, textoGasActual);
  Udp.beginPacket(servidorIP, 8000);
  Udp.write("temp = ");
  Udp.write(textoTempActual);
  Udp.write(",");
  Udp.write("ruido = ");
  Udp.write(textoRuidoActual);
  Udp.write(",");
  Udp.write("gas = ");
  Udp.write(textoGasActual);
  Serial.print("Enviando paquete: ");
  Serial.print(textoTempActual);
  Serial.print(",");
  Serial.println(textoRuidoActual);
  Udp.endPacket();
}

float getTemperatura() {
  sensors.requestTemperatures();
  return sensors.getTempCByIndex(0);
}

float getRuido() {
  return analogRead(A0);//*5.0/1024.0;
}

float gasExceed() {
  return digitalRead(gasPin);
}

void setWifi() {
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) { //Mientras no est&aacute; conectado.
    delay(250);
  }
}

