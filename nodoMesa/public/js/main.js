//var socket = io.connect('http://192.168.1.186:8080'); //Con localhost en raspi vale
var socket = io.connect('http://localhost:8080'); //Con localhost en raspi vale

socket.on("connect", function() {
    /*
        socket.on('messages', function(data) {
            console.log(data);
            render(data);
        });
    */
    socket.on('comanda', function(comanda) {
	console.log(comanda);
        render('comanda', comanda);
    });
    socket.on('comandasAntiguas', function(comandasAntiguas) {
        render('comandasAntiguas', comandasAntiguas);
    });

    socket.on('comandaACK', function() {
        alert("Comanda confirmada en cocina");

        //iniciar cuenta atrás
    });

    socket.on('comandaVacia', function() {
        alert("No me has pedido nada!");
    });

    socket.on('comandaLista', function() {
        alert("Tu pedido ya está listo!")
    });

    socket.on('cuentaEnCamino', function() {
        alert("La cuenta está en camino!")
    });

    socket.on('cuentaSolicitada', function() {
        alert("La cuenta está solicitada!")
    });

    socket.on('camareroEnCamino', function() {
        alert("El camarero está en camino!")
    });

    socket.on('camareroSolicitado', function() {
        alert("Ha solicitado un camarero!")
    });

    window.onbeforeunload = function() {
        socket.disconnect();
        socket.close();
    };

    socket.on('disconnect', function() {
        console.log('disconnect client event....');
        socket.disconnect();
        socket.close();
    });
});

function render(objeto, data) {
    if (objeto == "comanda") {
        renderComanda(data);
    } else if (objeto == "comandasAntiguas") {
        renderComandasAntiguas(data);
    }
}

function renderComandasAntiguas(data) {
    var precioTotal = 0;
    var html = data.map(function(elem, index) {
        var precio = parseFloat(elem.precio) * parseInt(elem.cantidad);
        precioTotal = parseFloat(precioTotal) + precio;
        precio = precio.toFixed(2);
        precioTotal = precioTotal.toFixed(2);
        console.log(precioTotal);
        var newRow = `<tr>
              <td>${elem.nombre}</td>
              <td>${elem.cantidad}</td>
              <td>.....</td>
              <td>` + precio.toString() + ` €</td>
            </tr>`;
        if (index == 0) {
            return (`<tr>
                <th>Comida</th>
                <th>Cantidad</th
                <th.....th>
                <th>Precio</th>
              </tr>` + newRow);
        }
        if (index == (data.length - 1)) {
            return (
                newRow +
                `<tr>
          <td>Total: </td><td></td><td></td><td>` + precioTotal.toString() + ` €</td>
        </tr>`
            );
        } else {
            return (newRow);
        }
    }).join(" ");

    document.getElementById('comandasAntiguas').innerHTML = html;
    /*
    TODO
	Dividir pagina en dos columnas y esta en la derecha y comandas en la izquierda
	La parte derecha no necesita botones, solo muestra lo que ya se ha pedido
    */
}

function renderComanda(data) {
    var precioTotal = 0;
    var html = data.map(function(elem, index) {
        var precio = parseFloat(elem.precio) * parseInt(elem.cantidad);
        precioTotal = parseFloat(precioTotal) + precio;
        precio = precio.toFixed(2);
        precioTotal = precioTotal.toFixed(2);
        console.log(precioTotal);
        var newRow = `<tr>
              <td>${elem.nombre}</td>
              <td>${elem.cantidad}</td>
              <td><button onclick="eliminarUno(&quot;${elem.id}&quot;)">-1</button></td>
              <td><button onclick="eliminarTodo(&quot;${elem.id}&quot;)">X</button></td>
              <td.....td>
              <td>` + precio.toString() + ` €</td>
            </tr>`;
        if (index == 0) {
            return (`<tr>
                <th>Comida</th>
                <th>Cantidad</th>
                <th></th>
                <th></th>
                <th.....th>
                <th>Precio</th>
              </tr>` + newRow);
        }
        if (index == (data.length - 1)) {
            return (
                newRow +
                `<tr>
          <td>Total: </td><td></td><td></td><td></td><td>` + precioTotal.toString() + ` €</td>
        </tr>`
            );
        } else {
            return (newRow);
        }
    }).join(" ");

    document.getElementById('comanda').innerHTML = html;

};

function eliminarUno(id) {
    socket.emit("quitarUno", id);
}

function eliminarTodo(id) {
    socket.emit("quitarTodo", id);
}

function enviarComanda() {
    socket.emit("nuevaComanda");
}

function solicitarCamarero() {
    socket.emit("solicitarCamarero");
}

function solicitarCuenta() {
    socket.emit("solicitarCuenta");
}

function testID() {
    socket.emit("testID");
}
