var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var dgram = require('dgram');
var servidor = dgram.createSocket('udp4');
var client = require('socket.io-client');
var nodoCentral = client.connect('http://192.168.1.201:8081', {
    reconnect: true
}); //191 alvaro
//var rc522 = require("rc522");

// Utilizar la dirección ip del nodoCentral

//Electron variables
const electron = require('electron');
const app_electron = electron.app;
const {
    BrowserWindow
} = require('electron');
let mainWindow;

app_electron.on('ready', function() {
    mainWindow = new BrowserWindow({
        autoHideMenuBar: true,
        width: 900,
        height: 600,
        minWidth: 450,
        minHeight: 300,
        icon: './public/img/restaurant.ico'
    });
    mainWindow.loadURL("http://localhost:8080/");
    mainWindow.setFullScreen(true);
    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    });

    mainWindow.on('closed', function() {
        mainWindow = null;
    });
});

app_electron.on('window-all-closed', function() {
    if (process.platform != 'darwin') {
        app_electron.quit();
    }
});


function rfid1() {
    var spawn = require("child_process").spawn;
    var p = spawn("python", ["/home/pi/lse_restaurante/nodoMesa/rc522.py", '0']);
    p.unref();
    p.stdout.on('data', function(rfidSerialNumber) {
        console.log(uint8arrayToString(rfidSerialNumber));
        rc522(uint8arrayToString(rfidSerialNumber), '1');
    });
    // Handle error output
    p.stderr.on('data', (data) => {
        // As said before, convert the Uint8Array to a readable string.
        console.log(uint8arrayToString(data));
    });
    console.log("rfid1");
}


function rfid2() {
    var spawn = require("child_process").spawn;
    var p = spawn("python", ["/home/pi/lse_restaurante/nodoMesa/rc522.py", '1']);
    p.unref();
    p.stdout.on('data', function(rfidSerialNumber) {
        console.log(uint8arrayToString(rfidSerialNumber));
        rc522(uint8arrayToString(rfidSerialNumber), '2');
    });
    // Handle error output
    p.stderr.on('data', (data) => {
        // As said before, convert the Uint8Array to a readable string.
        console.log(uint8arrayToString(data));
    });
    console.log("rfid2");
}


app.use(express.static('public'));

var webSocket;

function newComanda(id, nombre, precio) {
    this.id = id;
    this.nombre = nombre;
    this.cantidad = 1;
    this.precio = precio
}

function Sensor() {
    this.temperatura = 0;
    this.ruido = 0;
    this.gas = 0;
}

var datosSensores = new Sensor;

var carta = [];

var comanda = [];

var comandasAntiguas = [];

rfid1();
rfid2();

// Function to convert an Uint8Array to a string
var uint8arrayToString = function(data) {
    return String.fromCharCode.apply(null, data);
};

function rc522(rfidSerialNumber, which) {
    //console.log("rc522 funcion");
    //console.log("rfidSerialNumber " + rfidSerialNumber.substring(0,8));
    rfidSerialNumber = rfidSerialNumber.substring(0,8);	

    for (var i = 0; i < carta.length; ++i) {
	//console.log("carta[i].etiqueta " + carta[i].etiqueta);

        if (carta[i].etiqueta == rfidSerialNumber && carta[i].disponibilidad == 1) {
            if (comanda.length < 1) {
                comanda.push(new newComanda(carta[i].id, carta[i].nombre, carta[i].precio));
            } else {
                var flag = 0;
                for (var j = 0; j < comanda.length; ++j) {
                    if (comanda[j].id == carta[i].id) {
                        flag = 1;
                        comanda[j].cantidad = comanda[j].cantidad + 1;
                    }
                }
                if (flag == 0) {
                    comanda.push(new newComanda(carta[i].id, carta[i].nombre, carta[i].precio));
                }
            }
        }
    }
    if (webSocket != null) {
        webSocket.emit('comanda', comanda);
	console.log("comanda leida");
    }
    if (which == '1') {
        setTimeout(rfid1, 1500);
    } else {
        setTimeout(rfid2, 1500);
    }
}

nodoCentral.on('connect', function() {
    console.log("Conectado a servidor");
    nodoCentral.emit('mesa', 1); //Cambiar en diferentes nodoMesa
});

nodoCentral.on('carta', function(data) {
    carta = data;
    console.log(carta);
});

nodoCentral.on('comandaACK', function() {
    webSocket.emit('comandaACK');
});

nodoCentral.on('comandaLista', function() {
    webSocket.emit('comandaLista');
});

nodoCentral.on('camareroSolicitado', function() {
    webSocket.emit('camareroSolicitado');
});

nodoCentral.on('camareroEnCamino', function() {
    webSocket.emit('camareroEnCamino');
});

nodoCentral.on('cuentaSolicitada', function() {
    webSocket.emit('cuentaSolicitada');
});

nodoCentral.on('cuentaEnCamino', function() {
    webSocket.emit('cuentaEnCamino');
});

nodoCentral.on('mesaLimpia', function() {
    console.log("mesa limpia");
    comanda = [];
    comandasAntiguas = [];
    webSocket.emit('comanda', comanda);
    webSocket.emit('comandasAntiguas', comandasAntiguas);
});

io.on('connection', function(socket) {

    webSocket = socket;

    webSocket.emit('comanda', comanda);

    webSocket.on('quitarUno', function(id) {
        console.log("Queremos quitar uno!  " + id);
        for (var j = comanda.length - 1; j >= 0; --j) {
            if (comanda[j].id == id) {
                comanda[j].cantidad = comanda[j].cantidad - 1;
                if (comanda[j].cantidad == 0) {
                    comanda.splice(j, 1);
                }
            }
        }
        webSocket.emit('comanda', comanda);
        console.log(comanda);
    });

    webSocket.on('quitarTodo', function(id) {
        console.log("Queremos quitar todo!  " + id);
        for (var j = comanda.length - 1; j >= 0; --j) {
            if (comanda[j].id == id) {
                comanda.splice(j, 1);
            }
        }
        webSocket.emit('comanda', comanda);
        console.log(comanda);
    });

    webSocket.on('nuevaComanda', function() {
        if (comanda.length == 0) {
            webSocket.emit('comandaVacia');
        } else {
            nodoCentral.emit('nuevaComanda', comanda);
            //comandasAntiguas.push(comanda);
            for (var i = 0; i < comanda.length; ++i) {
                var flag = false;
                var j = 0;
                while (!flag && j < comandasAntiguas.length) {
                    if (comandasAntiguas[j].id == comanda[i].id) {
                        flag = true;
                        comandasAntiguas[j].cantidad += comanda[i].cantidad;
                    }
                    ++j;
                }
                if (!flag) {
                    comandasAntiguas.push(comanda[i]);
                }
                //comandasAntiguas.push(comanda[i]);
            }
            console.log("Comandas Antiguas: " + comandasAntiguas);
            comanda = [];
            webSocket.emit('comanda', comanda);
            webSocket.emit('comandasAntiguas', comandasAntiguas);
        }
    });

    webSocket.on('solicitarCuenta', function() {
        console.log("solicitando cuenta");
        nodoCentral.emit('solicitarCuenta');
    });


    webSocket.on('solicitarCamarero', function() {
        console.log("solicitando camarero");
        nodoCentral.emit('solicitarCamarero');
    });

    webSocket.on('testID', function() {
        console.log("Test RFID");
        var rfid = Math.floor((Math.random() * 10) + 1);
        console.log(rfid);
        rc522(rfid, '1');
    });

    socket.on('disconnect', function() {
        console.log('user disconnected');
        socket.disconnect();
        //socket.close();
    });
});

server.listen(8080, function() {
    console.log("Servidor corriendo en http://localhost:8080"); //186
});

servidor.on('message', function(msg, rinfo) {
    console.log('Server got: ' + msg + ' from ' + rinfo.address + ':' + rinfo.port);
    var mensaje = String(msg).split(',');
    mensaje.forEach(elem => {
        if (elem.includes("temp")) {
            datosSensores.temperatura = elem.replace("temp = ", "");
        } else if (elem.includes("ruido")) {
            datosSensores.ruido = elem.replace("ruido = ", "");
        } else if (elem.includes("gas")) {
            datosSensores.gas = elem.replace("gas = ", "");
        };
    });
    console.log(datosSensores);
    nodoCentral.emit('sensores', datosSensores);
});

servidor.on('error', function(err) {
    console.log('server error:\n' + err.stack);
    servidor.close();
});

servidor.on('listening', function() {
    var address = servidor.address();
    console.log('server listening ' + address.address + ":" + address.port);
});

servidor.bind({
    address: '192.168.1.176', //186
    port: 8000,
    exclusive: true
});
