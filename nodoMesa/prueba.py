import RPi.GPIO as GPIO
import MFRC522
import signal
import sys

GPIO.setwarnings(False)

RFID1 = MFRC522.MFRC522('/dev/spidev1.' + sys.argv[1])
RFID2 = MFRC522.MFRC522('/dev/spidev1.' + sys.argv[2])

def main():

    ident = ""
    while ident == "":
        (status,TagType) = RFID1.MFRC522_Request(RFID1.PICC_REQIDL)

            # Get the UID of the card
        (status,uid) = RFID1.MFRC522_Anticoll()

        if status == RFID1.MI_OK:
                ident = ""
                for i in uid:
                    if i < 10:
                        ident = ident + '0' + (hex(i)[2:])
                    else:
                        ident = ident + (hex(i)[2:])
                print ident

        (status2,TagType2) = RFID2.MFRC522_Request(RFID2.PICC_REQIDL)

            # Get the UID of the card
        (status2,uid2) = RFID2.MFRC522_Anticoll()

        if status2 == RFID2.MI_OK:
                ident = ""
                for i in uid:
                    if i < 10:
                        ident = ident + '0' + (hex(i)[2:])
                    else:
                        ident = ident + (hex(i)[2:])
                print ident

# Start process
if __name__ == '__main__':
    main()
