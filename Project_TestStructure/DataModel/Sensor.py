#!/usr/bin/env python

######################
#CLASS DOCUMENTATION
######################

#This class will define the Sensor object.
#Each Sensor object will represent a sensor of the Node.
#The Sensor object will contain the following fields:
# 1) Sensor_ID: identification of the node connected to the sensor
# 2) Node_ID: identification of the node connected to the sensor
# 3) Sensor_Type: type of sensor
#Sensor objects will be used within Node functions. Also within Node-Server communications??

######################################

class Sensor:
    
    def __init__(self, id, node, type):
        self.Sensor_ID = id
        self.Sensor_Type = type
        self.Node_ID = node
