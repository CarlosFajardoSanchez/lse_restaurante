#!/usr/bin/env python

######################
#CLASS DOCUMENTATION
######################

#This class will define the Order object.
#Each Order object will represent an order (X items requested from the menu).
#The Order object will contain the following fields:
# 1) Order_ID: identification of the order
# 2) Table_ID: identification of the Table that requests the order
# 3) items: list of Item objects with their ID, quantity and price
# 4) Order_State: state of the Order
#       We will define four states for the Order: REQUESTED, READY, SERVED, PAYED
#Orders will be transmitted between one Table and the Server.
#The communication will be realized by serialization and deserialization of the Order object.

######################################

import Item

class Order:

    def __init__(self, order_id, table_id, order_items):
        self.Order_ID = order_id
        self.Table_ID = table_id
        self.items = order_items
        self.Order_State = "REQUESTED"

    def AddItem (self, item):
        self.items.append(item)

    def RemoveItem (self, item):
        self.items.remove(item)

    def ChangeOrderState (self, state):
        self.Order_State = state
