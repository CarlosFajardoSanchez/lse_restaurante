#!/usr/bin/env python

######################
#CLASS DOCUMENTATION
######################

#This class will define the Item object.
#Each Item object will represent an item from the menu
#The Item object will contain the following fields:
# 1) Item_ID: identification of the order
# 2) Quantity: identification of the Table that requests the order
# 3) Price: list of Item objects with their ID, quantity and price
#Items will be used within an Order object.

######################################

class Item:

    def __init__(self, id, quantity, price):
        self.Item_ID = id
        self.Quantity = quantity
        self.Price = price
