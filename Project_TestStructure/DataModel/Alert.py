#!/usr/bin/env python

######################
#CLASS DOCUMENTATION
######################

#This class will define the Alert object.
#Each Alert object will represent an alert (X items requested from the menu).
#The Alert object will contain the following fields:
# 1) Alert_Type: type of the alert
# 2) Message: message of the alert if necessary
#Alerts will be transmitted between the Server and one/more Tables.
#The communication will be realized by serialization and deserialization of the Alert object.

######################################

class Alert:
    
    def __init__(self, type, message):
        self.Alert_Type = type
        self.Message = message
