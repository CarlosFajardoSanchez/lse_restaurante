//var socket = io.connect('http://192.168.1.201:8080');
var socket = io.connect('http://localhost:8080'); // IP de la Raspberry

// Expresion regular: numero con dos digitos
var re = new RegExp('^[0-9]+(\.[0-9][0-9]?)?$');

var ultimoRFIDLeido = 0;
//var globaldata;

function Plato(nombre, precio, etiqueta) {
    this.id = "";
    this.nombre = nombre;
    this.precio = precio;
    this.etiqueta = etiqueta;
    this.disponibilidad = 1;
}

socket.on("connect", function(data) {
    console.log("connect");
    socket.emit('web', 1);

    socket.on('renderComanda', function(pedidosPendientes) {
        render('comanda', pedidosPendientes);
    });

    socket.on('renderCarta', function(carta) {
        console.log(carta);
        render('carta', carta);
    });

    socket.on('rfidLeido', function(rfidLeido) {
        ultimoRFIDLeido = rfidLeido;
        render('rfid', rfidLeido);
    });

    socket.on('sensoresMesa', function(sensoresMesa) {
        render('sensores', sensoresMesa);
    });

    socket.on('renderCamareros', function(camareros) {
        render('camarero', camareros);
    });

    socket.on('renderCuentas', function(cuentas) {
        render('cuenta', cuentas);
    });
});

window.onbeforeunload = function() {
    //socket.close();
    socket.disconnect();
}

socket.on('disconnect', function() {
    console.log('disconnect client event....');
    //socket.close();
    socket.disconnect();
});

function render(objeto, data) {
    if (objeto == "comanda") {
        renderComanda(data);
    } else if (objeto == "rfid") {
        renderRFID(data);
    } else if (objeto == "carta") {
        renderCarta(data);
    } else if (objeto == "sensores") {
        renderSensores(data);
    } else if (objeto == "camarero") {
        renderCamarero(data);
    } else if (objeto == "cuenta") {
        renderCuenta(data);
    }
}

function renderCarta(data) {
    // La carta es un array de platos

    console.log("render carta");
    console.log(data);
    // Mostrar carta
    var auxHtml = data.map(function(elem, index) {
        var auxRows = ` `;

        // Cambiar ${index} por el identificador (unico) del plato
        auxRows += `<tr>
						<td>${elem.nombre}</td>
						<td>${elem.precio}</td>
						<td>${elem.etiqueta}</td>
						<td>${elem.disponibilidad}</td>
						<td><button onclick="cambiarDisponibilidad(${index})">Cambiar disponibilidad</button></td>
						<td><button onclick="cambiarPrecio(${index})">Cambiar precio</button></td>
						<td><button onclick="eliminarPlato(${index})">Eliminar plato</button></td>
					</tr>`;
        return auxRows;
    }).join(" ");

    var newTable = `<table
						<tr>
							<th>Nombre</th>
							<th>Precio</th>
							<th>Etiqueta RFID</th>
							<th>Disponibilidad</th>
						</tr>
					` + auxHtml + `
					</table>`;

    document.getElementById('cartaActual').innerHTML = newTable;
}

function renderRFID(data) {
    var html = "RFID: ";
    if (data == 0) {
        html += "No se ha leido";
    } else if (data == -1) {
        html += "Esperando...";
    } else {
        html += data;
    }
    document.getElementById('rfidValue').innerHTML = html;
}

function renderCamarero(data) {

    var html = data.map(function(elem, index) {
        var newTable = `<table>
							<tr>
								<th>Mesa ${elem} ha solicitado un camarero</th>
								<th><button onclick="eliminarCamarero(${elem})">Enviar camarero</button></th>
							</tr>
						</table>`;
        return newTable;
    }).join(" ");

    document.getElementById('camareros').innerHTML = html;
}

function renderCuenta(data) {

    var html = data.map(function(elem, index) {
        var newTable = `<table>
							<tr>
								<th>Mesa ${elem} ha solicitado la cuenta</th>
								<th><button onclick="eliminarCuenta(${elem})">Pagado</button></th>
							</tr>
						</table>`;
        return newTable;
    }).join(" ");

    document.getElementById('cuentas').innerHTML = html;
}

function renderComanda(data) {
    var html = data.map(function(elem, index) {
        var auxRows = ` `;
        for (var i = 0; i < elem.comanda.length; ++i) {
            auxRows += `<tr>
							<td>${elem.comanda[i].nombre}</td>
							<td>${elem.comanda[i].cantidad}</td>
						</tr>`
        }
        var newTable = `<table>
							<tr>
								<th>Mesa ${elem.mesa}</th>
								<th><button onclick="eliminarComanda(${index}, ${elem.mesa})">X</button></th>
							</tr>
							` + auxRows + `
						</table>`;
        return newTable;
    }).join(" ");

    document.getElementById('pedidosPendientes').innerHTML = html;
}

function renderSensores(data) {
    // Mostrar Sensores
	//globaldata = data;
	//console.log("Data sensores" + data);
    var auxHtml = data.map(function(elem, index) {
        var auxRows = ` `;

        // Cambiar ${index} por el identificador (unico) del plato
        auxRows += `<tr>
						<td>${elem.mesa}</td>
						<td>${elem.temperatura}</td>
						<td>${elem.ruido}</td>
						<td>${elem.gas}</td>
					</tr>`;
        return auxRows;
    }).join(" ");

    var newTable = `<table>
						<tr>
							<th>Datos de sensores</th>
						</tr>
						<tr>
							<td>Mesa</td>
							<td>Temperatura</td>
							<td>Ruido</td>
							<td>Gas</td>
						</tr>
					` + auxHtml + `
					</table>`;

    document.getElementById('sensores').innerHTML = newTable;
}

//Funciones de botones

function eliminarComanda(index, mesa) {
    socket.emit('eliminarComanda', index, mesa);
}

function cambiarDisponibilidad(index) {
    socket.emit('cambiarDisponibilidad', index);
}

function cambiarPrecio(index) {
    var nuevoPrecio = document.getElementById("nuevoPrecio").value;
    var esPrecio = re.test(nuevoPrecio);
    console.log("nuevo precio: " + nuevoPrecio);
    if (esPrecio && nuevoPrecio != "") {
        document.getElementById("nuevoPrecio").value = "";
        socket.emit('cambiarPrecio', index, nuevoPrecio);
    } else {
        if (!esPrecio) {
            alert("Nuevo precio incorrecto");
        } else if (nuevoPrecio == "") {
            alert("Falta el precio nuevo");
        }
    }
}

function eliminarPlato(index) {
    socket.emit('eliminarPlato', index);
}

function eliminarCamarero(index) {
    socket.emit('eliminarCamarero', index);
}

function eliminarCuenta(index) {
    socket.emit('eliminarCuenta', index);
}


function openTab(e, tabSelect) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabSelect).style.display = "block";
    e.currentTarget.className += " active";
}

function leerRFID() {
    console.log("leerRFID");
    socket.emit('leerRFID');
    render('rfid', -1);
}

function guardarPlato() {
    var nombre = document.getElementById("nombrePlato").value;
    var precio = document.getElementById("precioPlato").value;
    var esPrecio = re.test(precio);
    if (esPrecio && precio != "" && nombre != "" && ultimoRFIDLeido != 0) {
        var plato = new Plato(nombre, precio, "" + ultimoRFIDLeido);
        ultimoRFIDLeido = 0;
        console.log(plato);
        socket.emit('guardarPlato', plato);

        //Vaciar los campos leidos
        document.getElementById("nombrePlato").value = "";
        document.getElementById("precioPlato").value = "";
        document.getElementById('rfidValue').innerHTML = "RFID: ";

        document.getElementById('textNombrePlato').innerHTML = "";
        document.getElementById('textPrecio').innerHTML = "";

    } else {
        alert("Faltan datos");
    }
}

function mandarCartaMesas() {
    socket.emit('mandarCartaMesas');
}

function nombrePlatoChange() {
    var nombre = document.getElementById("nombrePlato");
    var html = "";
    html += nombre.value + " ";
    document.getElementById('textNombrePlato').innerHTML = html;
    precioChange();
}

function precioChange() {
    var precio = document.getElementById("precioPlato");
    var esPrecio = re.test(precio.value);
    if (!esPrecio) {
        precio.style.backgroundColor = "rgba(255, 0, 0, 0.3)";
        document.getElementById('textPrecio').innerHTML = "";
    } else {
        precio.style.backgroundColor = "";
        if (document.getElementById("nombrePlato").value != "" && precio.value != "") {
            var html = "";
            html += "con precio: " + precio.value + "€.";
            document.getElementById('textPrecio').innerHTML = html;
        } else {
            document.getElementById('textPrecio').innerHTML = "";
        }
    }
}

function nuevoPrecioChange() {
    var precio = document.getElementById("nuevoPrecio");
    var esPrecio = re.test(precio.value);
    if (!esPrecio) {
        precio.style.backgroundColor = "rgba(255, 0, 0, 0.3)";
    } else {
        precio.style.backgroundColor = "";
    }
}


function addMessage(e) {
    var message = {
        text: document.getElementById('texto').value
    };

    socket.emit('new-message', message);
    return false;
};
