##########################
README - Server
##########################

1. Database

La base de datos se va a instanciar solamente una vez en el server del nodoCentral. Todas las comunicaciones con la BD se deben hacer usando la interfaz exportada por el m�dulo database.js. As� tenemos el c�digo dependiente de MongoDB separado del resto de la interfaz cliente-servidor. Si hacen falta m�s metodos se a�aden ah� usando el mismo esquema ("exports.metodoPatata = function()")

Pasos previos para poder utilizar la BD:
1) Instalar MongoDB para el SO de la m�quina (descargar e instalar, todo en configuraci�n automatica)
2) Driver mongodb de nodejs --> ya est� instalado y con las dependencias a�adidas --> no hace falta
3) Crear la DB: 
	MongoDB genera una pila de ficheros en /data una vez conectada la BD por primera vez, y los actualiza cada vez que realizas 	operaciones sobre ella. No es 100% seguro, pero lo m�s normal es que var�en en funci�n del SO de la m�quina, por lo que no 	est�n en el repo.

	Para poderla crear, abrir un terminal/cmd en nodoCentral y ejecutar: 	npm start
	El comando abrir� una linea de comandos de mongodb que iniciar� el server de MongoDB (obligatorio para poder operar con la 		base de datos, solo con el driver de nodejs no vale). 
	@TODO: modificar package.json para que con el mismo comando "npm start" se ejecute el main.js del servidor nodoCentral 		(actualmente, se puede modificar simplemente con una l�nea adicional, pero no se conecta correctamente a la BD para hacer 	operaciones)

	NO ES NECESARIO CREAR LAS TABLAS (usando el m�todo crearDB()). Estas se generan autom�ticamente cuando se inserta algo por 	primera vez.
4) var database = require('./database.js'); en el c�digo donde haya que usarlo