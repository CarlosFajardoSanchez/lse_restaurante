////////////////////////////////////////////////////
//DATABASE.JS -- INTERFACE WITH MONGODB DATABASE
////////////////////////////////////////////////////
//Use with: var database = require('./database.js');
////////////////////////////////////////////////////

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
const url = 'mongodb://localhost:27017/restaurante';

//SIEMPRE hay que arrancarlo antes del main
//"mongod --dbpath=./data --port 27017 --logpath=./log/database.log" es el comando para ejecutar el server de mongodb
//En el package.json hay un script de arranque --> en cmd/terminal --> npm start
//La ruta relativa de log deberia reconocerla --> si no, cambiar a absoluta

//Plato - Modelo de datos (Objeto)
function plato(id, nombre, precio, etiqueta, disponibilidad) {
    this.id = id; //deberá ser un ObjectID!!
    this.nombre = nombre;
    this.precio = precio;
    this.etiqueta = etiqueta;
    this.disponibilidad = disponibilidad;
};

//Sensor - Modelo de datos (Objeto)
//sensor.datos deberá tener el último dato del sensor (o + de uno??)
function sensor(id, mesa, temperatura, ruido) {
    this.id = id; //deberá ser un ObjectID!!
    this.mesa = mesa;
    this.temperatura = temperatura;
    this.ruido = ruido;
};

//pasar a string el ObjectID que genera la BD
exports.parseObjectID = function(id) {
    return id.toHexString();
};

//Generación de la base de datos
//Ya está generada en el repo
exports.crearDB = function() {
    //creamos base de datos
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("DB creada!");
        db.close();
    });

    //creamos tablas para la carta y los sensores
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.createCollection("carta", function(err, res) {
            assert.equal(null, err);
            console.log("Tabla generada: Carta");
            db.close();
        });

        db.createCollection("sensor", function(err, res) {
            assert.equal(null, err);
            console.log("Tabla generada: Sensor");
            db.close();
        });
    });
};

//prueba de conexion
exports.probarConexion = function() {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("Conexión correcta con el servidor!");
        db.close();
    });
};

//recuperar toda la carta
exports.recuperarCarta = function() {
    var carta = [];
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.collection("carta").find({}).toArray(function(err, result) {
            assert.equal(null, err);
            var item;
            for (var i = 0; i < result.length; i++) {
                item = new plato(result[i]._id, result[i].nombre, result[i].precio, result[i].etiqueta, result[i].disponibilidad);
                carta.push(item);
            };
            console.log("Carta actual: \n" + carta);
            db.close();
        });
    });
    return carta;
};

//añadir plato
//devuelve el identificador del plato que asigna la DB
//después en el server hay que actualizar el plato con el ObjectID que devuelve la llamada a la BD
exports.añadirPlato = function(plato) {
    var id_plato = new ObjectID();
    console.log("Generamos su identificador: " + id_plato.toHexString());
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var nuevoPlato = {
            "_id": id_plato,
            "nombre": plato.nombre,
            "precio": plato.precio,
            "etiqueta": plato.etiqueta,
            "disponibilidad": plato.disponibilidad
        };
        db.collection("carta").insertOne(nuevoPlato, function(err, res) {
            assert.equal(null, err);
            console.log("Plato añadido: " + nuevoPlato);
            db.close();
        });
    });
    return id_plato;
};

//quitar plato
exports.quitarPlato = function(plato) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var removalQuery = {
            "_id": plato.id,
            "nombre": plato.nombre
        };
        db.collection("carta").remove(removalQuery, function(err, res) {
            assert.equal(null, err);
            console.log("Plato eliminado: " + res);
            db.close();
        });
    });
};

//actualizar plato
exports.actualizarPlato = function(plato) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var updateQuery;
        db.collection("carta").updateOne({
            "_id": plato.id
        }, {
            $set: {
                "disponibilidad": plato.disponibilidad,
                "nombre": plato.nombre,
                "precio": plato.precio,
                "etiqueta": plato.etiqueta
            }
        }, function(err, result) {
            assert.equal(null, err);
            console.log("Plato actualizado: " + result);
            db.close();
        });
    });
};

//añadir sensor
//devuelve el identificador del sensor que asigna la DB
//después en el server hay que actualizar el sensor con el ObjectID que devuelve la llamada a la BD
exports.añadirSensor = function(sensor) {
    var id_sensor = new ObjectID();
    console.log("Generamos su identificador: " + id_sensor.toHexString());
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var nuevoSensor = {
            "_id": id_sensor,
            "mesa": sensor.mesa,
            "temperatura": sensor.temperatura,
            "ruido": sensor.ruido
        };
        db.collection("sensor").insertOne(nuevoSensor, function(err, res) {
            assert.equal(null, err);
            console.log("Sensor añadido: " + nuevoSensor);
            db.close();
        });
    });
    return id_sensor;
};

//añadir valor
exports.añadirValorSensor = function(sensor) {
    var valoresTemperatura = [];
    var valoresRuido = [];
    //recuperamos el valor actual
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var retrieveQuery = {
            "_id": sensor.id,
            "mesa": sensor.mesa
        };

        db.collection("sensor").find({}).toArray(function(err, result) {
            valoresTemperatura = result[0].temperatura;
            valoresRuido = result[0].ruido;
            console.log("Historico del sensor recuperado: " + result);
        });
        /*
                db.collection("sensor").find(retrieveQuery, {
                    "temperatura": 1,
                    "ruido": 1,
                    "mesa": 0,
                    _id: 0
                }, function(err, res) {
                    assert.equal(null, err);
                    // Nota: res no es un sensor, es un cursor (variable de la base de datos)
                    valoresTemperatura = res[0].temperatura;
                    valoresRuido = res[0].ruido;
                    console.log("Historico del sensor recuperado: " + res);
                });
        */
        //añadimos los nuevos
        valoresTemperatura = valoresTemperatura.push(sensor.temperatura);
        valoresRuido = valoresRuido.push(sensor.ruido);
        db.collection("sensor").updateOne({
            "_id": sensor.id,
            "mesa": sensor.mesa
        }, {
            $set: {
                "temperatura": valoresTemperatura,
                "ruido": valoresRuido
            }
        }, function(err, res) {
            assert.equal(null, err);
            console.log("Sensor actualizado: " + res);
            db.close();
        });
    });
};

//recuperar historico sensor (los últimos numberOfValues valores)
exports.recuperarHistorico = function(sensor, numberOfValues) {
    var historico_sensor_temperatura, historico_sensor_ruido, historico_sensor;
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var retrieveQuery = {
            "_id": sensor.id,
            "mesa": sensor.mesa
        };
        db.collection("sensor").find(retrieveQuery, {
            "temperatura": 1,
            "ruido": 1,
            "tipo": 0,
            _id: 0
        }).toArray(function(err, result) {
            assert.equal(null, err);
            historico_sensor_temperatura = result[0].temperatura;
            historico_sensor_ruido = result[0].ruido;

            historico_sensor = [historico_sensor_ruido.slice(historico_sensor_ruido.length - 1 - numberOfValues, historico_sensor_ruido.length),
                historico_sensor_temperatura.slice(historico_sensor_temperatura.length - 1 - numberOfValues, historico_sensor_temperatura.length)
            ]

            console.log("Mapa actual de sensores: \n" + result);
            db.close();
        });
    });
    return historico_sensor;
};

//recuperar todo el mapa de sensores
//IMPORTANTE: se recupera todo el historico de todos los sensores!
//Si se quiere el ultimo valor hacerlo en el main.js
exports.recuperarSensores = function() {
    var mapa_sensores, temperatura, ruido;
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.collection("sensor").find({}).toArray(function(err, result) {
            assert.equal(null, err);
            var sensor_item;
            for (var i = 0; i < result.length; i++) {
                sensor_item = new sensor(result[i]._id, result[i].tipo, result[i].temperatura, result[i].ruido);
                mapa_sensores[i] = sensor_item;
            };
            console.log("Mapa actual de sensores: \n" + result);
            db.close();
        });
    });
    return mapa_sensores;
};

//eliminar sensor
exports.eliminarSensor = function(sensor) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        var removalQuery = {
            "_id": sensor.id,
            "mesa": sensor.tipo
        };
        db.collection("sensor").remove(removalQuery, function(err, res) {
            assert.equal(null, err);
            console.log("Sensor eliminado: " + sensor);
            db.close();
        });
    });
};
