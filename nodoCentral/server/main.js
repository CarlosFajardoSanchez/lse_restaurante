var express = require('express');
var app = express();
var serverMesa = require('http').Server(app);
var serverWeb = require('http').Server(app);
var ioMesa = require('socket.io')(serverMesa);
var ioWeb = require('socket.io')(serverWeb);
var dgram = require('dgram');
var servidor = dgram.createSocket('udp4');
var csv = require('ya-csv');
var rc522 = require("rc522");

//Electron variables
const electron = require('electron');
const app_electron = electron.app;
const {
    BrowserWindow
} = require('electron');
let mainWindow;

//path desde donse se ejecuta node ...
var database = require('./database.js');

function Pedido(mesa, comanda) {
    this.mesa = mesa;
    this.comanda = comanda; //array
}

function Comanda(id, nombre, cantidad) {
    this.id = id;
    this.nombre = nombre;
    this.cantidad = cantidad;
}

function Sensor(id, mesa, temperatura, ruido, gas) {
    this.id = id;
    this.mesa = mesa;
    this.temperatura = temperatura;
    this.ruido = ruido;
    this.gas = gas;
}

function Plato(id, nombre, precio, etiqueta, disponibilidad) {
    this.id = id;
    this.nombre = nombre;
    this.precio = precio;
    this.etiqueta = etiqueta;
    this.disponibilidad = disponibilidad;
}

// Sockets
var socketMesa = [];
var socketWeb;

// Mesas que existen
var mesasExistentes = [];

// Sensores de mesas - Mesa 5 tiene el sensor en indice 5
var sensoresMesa = [];

// Carta para mandar a las mesas
var carta = [];
//var comandas = [];

// Datos para llevar cuenta los pedidos pendientes y servidos
var pedidosPendientes = [];
var pedidosServidos = [];

// Mesas que han solicitado camarero
var camareros = [];
var cuentas = [];

app.use(express.static('public'));

var flagLeyendoRfid = 0;
var timeout;

app_electron.on('ready', function() {
    mainWindow = new BrowserWindow({
        autoHideMenuBar: true,
        width: 900,
        height: 600,
        minWidth: 450,
        minHeight: 300,
        icon: './public/img/restaurant.ico',
    });
    mainWindow.loadURL("http://localhost:8080/");
    mainWindow.maximize();

    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    })

    mainWindow.on('closed', function() {
        mainWindow = null;
    });
});

app_electron.on('window-all-closed', function() {
    if (process.platform != 'darwin') {
        app_electron.quit();
    }
});

rc522(function(rfidSerialNumber) {
    console.log("rfid leido " + rfidSerialNumber);
    if (flagLeyendoRfid == 1) {
        clearTimeout(timeout);
        var rfidLeido = rfidSerialNumber.replace("InitRc522", "");
        flagLeyendoRfid = 0;
        if (socketWeb) {
            console.log("rfid enviado");
            socketWeb.emit('rfidLeido', rfidLeido)
        }
    }
});

leerCarta();


// Recepciones de las mesas
ioMesa.on('connection', function(socket) {
    console.log("nueva mesa");

    // Primera conexion de la mesa
    socket.on('mesa', function(mesa) {
        socketMesa[mesa] = socket;
        var sensorNuevo = new Sensor(0, mesa, 0, 0, 0);
        var idSensor = añadirSensor(sensorNuevo);
        sensorNuevo.id = idSensor;
        sensoresMesa.push(sensorNuevo);

        // Guardo la mesa en mesas que existen
        //mesasExistentes.push(mesa);

        // Le mando la carta a la mesa
        socketMesa[mesa].emit('carta', carta);
    });

    // Recibo comanda nueva de una mesa
    socket.on('nuevaComanda', function(dataComanda) {
        var mesaIdx = -1;
        for (var i = 0; i < socketMesa.length; ++i) {
            if (socketMesa[i] == socket) {
                mesaIdx = i;
                //Salir del bucle
                //break;
            }
        }
        if (mesaIdx != -1) {
            console.log("Mesa " + mesaIdx);
            console.log(dataComanda);

            //Respuesta a la mesa
            socketMesa[mesaIdx].emit("comandaACK");

            //Mandar mensaje al cliente web para mostrar por pantalla
            var nuevaComanda = [];
            //console.log(dataComanda);
            for (var i = 0; i < dataComanda.length; ++i) {
                nuevaComanda.push(new Comanda(dataComanda[i].id, dataComanda[i].nombre, dataComanda[i].cantidad));
            }
            pedidosPendientes.push(new Pedido(mesaIdx, nuevaComanda));
            if (socketWeb) {
                socketWeb.emit("renderComanda", pedidosPendientes);
            }

        }
    });

    // Solicitan camarero
    socket.on('solicitarCamarero', function() {
        var mesaIdx = -1;
        for (var i = 0; i < socketMesa.length; ++i) {
            if (socketMesa[i] == socket) {
                mesaIdx = i;
                //Salir del bucle
                //break;
            }
        }
        if (mesaIdx != -1) {
            console.log("Mesa " + mesaIdx);
            console.log("Solicita camarero");

            //Respuesta a la mesa
            //socketMesa[mesaIdx].emit("camareroACK");


            if (socketWeb) {
                if (!camareros.includes(mesaIdx)) {
                    camareros.push(mesaIdx);
                    console.log("Camareros:");
                    console.log(camareros);
                    socketWeb.emit("renderCamareros", camareros);
                    socketMesa[mesaIdx].emit("camareroSolicitado");
                } else {
                    socketMesa[mesaIdx].emit("camareroEnCamino");
                }
            }

        }
    });

    // Solicitan cuenta
    socket.on('solicitarCuenta', function() {
        var mesaIdx = -1;
        for (var i = 0; i < socketMesa.length; ++i) {
            if (socketMesa[i] == socket) {
                mesaIdx = i;
                //Salir del bucle
                //break;
            }
        }
        if (mesaIdx != -1) {
            console.log("Mesa " + mesaIdx);
            console.log("Solicita cuenta");

            //Respuesta a la mesa
            //socketMesa[mesaIdx].emit("cuentaACK");


            if (socketWeb) {
                if (!cuentas.includes(mesaIdx)) {
                    cuentas.push(mesaIdx);
                    console.log("Cuentas: " + cuentas);
                    socketWeb.emit("renderCuentas", cuentas);
                    socketMesa[mesaIdx].emit("cuentaSolicitada");
                } else {
                    socketMesa[mesaIdx].emit("cuentaEnCamino");
                }
            }

        }
    });

    socket.on('sensores', function(dataSensor) {
	console.log("sensores " + dataSensor);
        var mesaIdx = -1;
        for (var i = 0; i < socketMesa.length; ++i) {
            if (socketMesa[i] == socket) {
                mesaIdx = i;
                //Salir del bucle
                //break;
            }
        }
        if (mesaIdx != -1) {

            var sensorIdx = -1;
            for (var i = 0; i < sensoresMesa.length; ++i) {
                if (sensoresMesa[i].mesa == mesaIdx) {
                    sensorIdx = i;
                    //Salir del bucle
                    //break;
                }
            }
            sensoresMesa[sensorIdx].temperatura = dataSensor.temperatura;
            sensoresMesa[sensorIdx].ruido = dataSensor.ruido;
	    sensoresMesa[sensorIdx].gas = dataSensor.gas;
            database.añadirValorSensor(sensoresMesa[sensorIdx]);
            if (socketWeb) {
		console.log("sensores hacia public");
                socketWeb.emit('sensoresMesa', sensoresMesa);
            }
        }
    });

    socket.on('disconnect', function() {
        console.log('user disconnected');
        socket.disconnect();
        //socket.close();
    });
});

// Recepciones de la pagina Web
ioWeb.on('connection', function(socket) {
    console.log("web connection");

    socket.on('web', function(junk) {
        //console.log("web recibida");
        console.log(carta);
        socketWeb = socket;
        socketWeb.emit("renderCarta", carta);
    });

    socket.on('disconnect', function() {
        //console.log('user disconnected');
        socket.disconnect();
    });

    //Deberia ir ioWEB ???
    socket.on('eliminarComanda', function(index, mesaIdx) {
        pedidosServidos.push(pedidosPendientes[index]);
        pedidosPendientes.splice(index, 1);
        socketWeb.emit("renderComanda", pedidosPendientes);
        socketMesa[mesaIdx].emit("comandaLista");
    });

    socket.on('leerRFID', function() {
        /*
                // Test
                var rfid = Math.floor((Math.random() * 10) + 1);
                socketWeb.emit("rfidLeido", rfid);
        */

        // Leer RFID y mandarlo
        console.log("leyendo rfid");
        flagLeyendoRfid = 1;
        // Esperar un tiempo, si se ejecuta el setTimeout entonces no se ha leido
        // En la lectura RFID se para el timeout y se envia el dato
        timeout = setTimeout(function() {
            // No se ha leido nada
            flagLeyendoRfid = 0;
            socketWeb.emit("rfidLeido", 0);
            console.log("rfid no leido");
        }, 2000);


    });

    socket.on('guardarPlato', function(platoRcv) {
        // Añadir plato a la carta TODO
        // Comprobar que nos dan un rfid distinto a los usados actualmente. Lo hace la database?
        var id = guardarPlato(platoRcv);
        carta.push(new Plato(id, platoRcv.nombre, platoRcv.precio, platoRcv.etiqueta, platoRcv.disponibilidad));
        //leerCarta();

        // Render carta visualizada
        console.log("carta actual");
        console.log(carta);
        socketWeb.emit("renderCarta", carta);

        // Mandar carta a todas las mesas
        //mandarCartaMesas();
    });

    socket.on('cambiarDisponibilidad', function(index) {
        //carta[index].disponibilidad = (carta[index].disponibilidad + 1) % 2;
        var platoActualizar = carta[index];
        if (platoActualizar.disponibilidad == 1) {
            platoActualizar.disponibilidad = 0;
        } else {
            platoActualizar.disponibilidad = 1;
        }
        actualizarPlato(platoActualizar);
        //leerCarta();

        // Render carta visualizada
        console.log("carta actual");
        console.log(carta);
        socketWeb.emit("renderCarta", carta);

        // Mandar carta a todas las mesas
        //mandarCartaMesas();
    });


    socket.on('cambiarPrecio', function(index, nuevoPrecio) {
        //carta[index].disponibilidad = (carta[index].disponibilidad + 1) % 2;
        var platoActualizar = carta[index];
        platoActualizar.precio = nuevoPrecio;
        actualizarPlato(platoActualizar);
        //leerCarta();

        // Render carta visualizada
        console.log("carta actual");
        console.log(carta);
        socketWeb.emit("renderCarta", carta);

        // Mandar carta a todas las mesas
        //mandarCartaMesas();
    });

    socket.on('eliminarPlato', function(index) {
        var plato = carta[index];
        eliminarPlato(plato);
        carta.splice(index, 1);
        //leerCarta();

        // Render carta visualizada
        console.log("carta actual");
        console.log(carta);
        socketWeb.emit("renderCarta", carta);

        // Mandar carta a todas las mesas
        //mandarCartaMesas();
    });

    socket.on('eliminarCamarero', function(index) {
        if (camareros.includes(index)) {
            for (var i = camareros.length - 1; i >= 0; --i) {
                if (camareros[i] == index) {
                    camareros.splice(i, 1);
                }
            }
        }
        socketWeb.emit("renderCamareros", camareros);

        /*
		for (var i = 0; i < socketMesa.length; ++i) {
            if (socketMesa[i] == socket) {
                mesaIdx = i;
                //Salir del bucle
                //break;
            }
        }
		socketMesa[mesaIdx].emit("camareroEnviado");
		*/
    });

    socket.on('eliminarCuenta', function(index) {
        if (cuentas.includes(index)) {
            for (var i = cuentas.length - 1; i >= 0; --i) {
                if (cuentas[i] == index) {
                    cuentas.splice(i, 1);
                }
            }
        }
        socketWeb.emit("renderCuentas", cuentas);
        console.log("Mesa limpia: " + index);
        socketMesa[index].emit("mesaLimpia"); //Limpiar los pedidos en la mesa

        for (var i = pedidosPendientes.length - 1; i >= 0; --i) {
            if (pedidosPendientes[i].mesa == index) {
                pedidosPendientes.splice(i, 1);
            }
        }
        socketWeb.emit("renderComanda", pedidosPendientes);

    });

    socket.on('mandarCartaMesas', function() {
        mandarCartaMesas();
    });
});

function mandarCartaMesas() {
    console.log("mandando carta a mesas");
    for (var i = 0; i < socketMesa.length; ++i) {
        if (socketMesa[i]) {
            socketMesa[i].emit('carta', carta);
        }
    }
}

// Funciones de la base de datos
function guardarPlato(plato) {
    //var id = database.añadirPlato(new Plato(platoRcv.nombre, platoRcv.precio, platoRcv.rfid, platoRcv.disponibilidad));
    var id = database.añadirPlato(plato);
    return id;
}

function actualizarPlato(plato) {
    var id = database.actualizarPlato
    return id;
}

function eliminarPlato(plato) {
    database.quitarPlato(plato);
}

function añadirSensor(sensor) {
    var id = database.añadirSensor(sensor);
    return id;
}

// -------------------------------------------------------

serverMesa.listen(8081, function() {
    console.log("Servidor corriendo en http://localhost:8081");
});

serverWeb.listen(8080, function() {
    console.log("Servidor corriendo en http://localhost:8080");
});

function leerCarta() {
    carta = database.recuperarCarta();

    /*
    var reader = csv.createCsvFileReader('./server/carta.csv', {
        'separator': ',', // Delimiter
    });
    reader.on('data', function(data) {
        //carta.push(new plato(data[0].trim(), data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim()));
        carta.push(new Plato(data[1].trim(), data[2].trim(), data[3].trim(), data[4].trim()));
    }).on('end', function() {
        //console.log(carta);
    });
	*/
}
